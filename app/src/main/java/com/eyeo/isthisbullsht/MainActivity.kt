package com.eyeo.isthisbullsht

import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.eyeo.isthisbullsht.api.RatingGroupsParser
import kotlinx.android.synthetic.main.activity_main.*
import java.net.URL
import javax.xml.parsers.SAXParserFactory

class MainActivity : AppCompatActivity() {

    private lateinit var handler: RatingGroupsParser
    private var shareIntent: Intent? = null
    private var rating = "Unknown"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val input = resources.openRawResource(R.raw.metacert)
        val factory = SAXParserFactory.newInstance()
        factory.isValidating = false
        val parser = factory.newSAXParser()
        handler = RatingGroupsParser()
        parser.parse(input, handler)

        Log.d("POOPY", "intent action ${intent?.action}")
        when (intent?.action) {
            Intent.ACTION_SEND-> loadRating(intent!!.getStringExtra(Intent.EXTRA_TEXT))
            Intent.ACTION_VIEW -> loadRating(intent!!.dataString)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        val ratingMenuItem = menu!!.findItem(R.id.menu_rating)

        val ratingMenuResId =
            when (rating.toRatingCategory()) {
                RatingCategory.GREEN -> R.drawable.green_poop
                RatingCategory.ORANGE -> R.drawable.orange_poop
                RatingCategory.RED -> R.drawable.red_poop
            }
        ratingMenuItem.setIcon(ratingMenuResId)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.menu_rating -> {
            Toast.makeText(this, "Rating: $rating", Toast.LENGTH_LONG).show()
            true
        }

        R.id.menu_share -> {
            if (shareIntent != null) startActivity(Intent.createChooser(shareIntent, getString(R.string.share_chooser_title)))
            true
        }

        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    private fun loadRating(urlStr: String) {
            val url = URL(urlStr)

            var urlPath = url.path
            if (urlPath == "/") urlPath = ""

            var urlHost = url.host
            urlHost = urlHost.replaceFirst("www.", "")
            urlHost = urlHost.replaceFirst("mobile.", "")

            Log.d("ITBS", "trying for $urlStr host: $urlHost path: $urlPath")

            val pathMap = handler.ratingMap[urlHost]
            if (pathMap != null) {
                val tempRating = pathMap[urlPath]
                if (tempRating != null) {
                    rating = tempRating
                }
            }

            invalidateOptionsMenu()
            
            showRatingDialog(urlStr)

            createShareIntent(urlStr, rating)
    }

    private fun createShareIntent(url: String, rating: String) {
         shareIntent = Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_TEXT, String.format(getString(R.string.share_menu_template), url, rating))
            type = "text/plain"
        }
    }

    private fun showRatingDialog(url: String) {
        webView.settings.javaScriptEnabled = true

        when(rating.toRatingCategory()) {
            RatingCategory.GREEN -> {
                dialogOverlay.setBackgroundColorResId(R.color.green_700_transparent)
                dialogTitle.setText(R.string.green_rating_dialog_title)
                dialogDescription.setText(R.string.green_rating_dialog_description)
                dialogDescription.setTextColorResId(R.color.green_700)
                webView.loadUrl(url)
            }
            RatingCategory.ORANGE -> {
                dialogOverlay.setBackgroundColorResId(R.color.orange_700_transparent)
                dialogTitle.setText(R.string.orange_rating_dialog_title)
                dialogDescription.setText(R.string.orange_rating_dialog_description)
                dialogDescription.setTextColorResId(R.color.orange_700)
                webView.loadUrl(url)
            }
            RatingCategory.RED -> {
                dialogOverlay.setBackgroundColorResId(R.color.red_900_transparent)
                dialogTitle.setText(R.string.red_rating_dialog_title)
                dialogDescription.setText(R.string.red_rating_dialog_description)
                dialogDescription.setTextColorResId(R.color.red_900)
                dialogContinueButton.visibility = View.VISIBLE
            }
        }

        dialogDismissButton.setOnClickListener { dialogOverlay.visibility = View.GONE }
        dialogContinueButton.setOnClickListener {
            webView.loadUrl(url)
            dialogOverlay.visibility = View.GONE
        }

        dialogOverlay.visibility = View.VISIBLE
    }
}

private fun String.toRatingCategory() =
    // GREEN: Trusted News
    // ORANGE: Biased, Satire, User generated content, Unknown
    // RED: Malware, Untrustworthy
    when (this) {
        "Trusted News" -> RatingCategory.GREEN
        "Biased", "Satire", "User generated content", "Unknown" -> RatingCategory.ORANGE
        else -> RatingCategory.RED
    }

private fun TextView.setTextColorResId(colorResId: Int) {
    setTextColor(ContextCompat.getColor(context, colorResId))
}

private fun View.setBackgroundColorResId(colorResId: Int) {
    setBackgroundColor(ContextCompat.getColor(context, colorResId))
}

private enum class RatingCategory { GREEN, ORANGE, RED }
