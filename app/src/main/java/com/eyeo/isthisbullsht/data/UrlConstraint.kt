package com.eyeo.isthisbullsht.data

data class UrlConstraint(val hosts: List<String>, val startingPaths: List<String>)